README (or not)

The provided lists work with common network appliances that utilize blocklists. We are sharing our blocklists with you for the purpose of transparency, so you can see what exactly we block from resolving over our VPN network as well as to allow you to use these lists for your own personal use.

These will be updated as needed. Please contact us if you feel that anything needs to be added, removed, or altered in any way.

If you want to experience the internet without trackers, malware, ads, and other "BS" you try out our barebones VPN service, available in 3 locations (USA, Luxembourg, & Ukraine with more locations soon) at https://incognet.io/vpn 

Enjoy.
IncogNet
